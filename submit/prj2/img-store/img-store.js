'use strict';

const Ppm = require('./ppm');

const assert = require('assert');
const fs = require('fs');
const os = require('os');
const path = require('path');
const {promisify} = require('util'); //destructuring
//const exec = promisify(require('child_process').exec);
const readFileAsync = promisify(fs.readFile);
const mongo = require('mongodb').MongoClient;

/** This module provides an interface for storing, retrieving and
 *  querying images from a database. An image is uniquely identified
 *  by two non-empty strings:
 *
 *    Group: a string which does not contain any NUL ('\0') 
 *           characters.
 *    Name:  a string which does not contain any '/' or NUL
 *           characters.
 *
 *  Note that the image identification does not include the type of
 *  image.  So two images with different types are regarded as
 *  identical iff they have the same group and name.
 *  
 *  Error Handling: If a function detects an error with a defined
 *  error code, then it must return a rejected promise rejected with
 *  an object containing the following two properties:
 *
 *    errorCode: the error code
 *    message:   an error message which gives details about the error.
 *
 *  If a function detects an error without a defined error code, then
 *  it may reject with an object as above (using a distinct error
 *  code), or it may reject with a JavaScript Error object as
 *  appropriate.
 */

function ImgStore(client, db) { //TODO: add arguments as necessary
  //console.log("image store constructor");
  this.client = client;
  this.db = db;
}

ImgStore.prototype.close = close;
ImgStore.prototype.get = get;
ImgStore.prototype.list = list;
ImgStore.prototype.meta = meta;
ImgStore.prototype.put = put;

/** Factory function for creating a new img-store.
 */
async function newImgStore() {
  const client = await mongo.connect(MONGO_URL);
  const db = client.db(DB_NAME);
  return new ImgStore(client, db); //provide suitable arguments
}
module.exports = newImgStore;

/** URL for database images on mongodb server running on default port
 *  on localhost
 */
const MONGO_URL = 'mongodb://localhost:27017';
const DB_NAME = 'images';
const DB_TABLE = 'imageInfo';
//List of permitted image types.
const IMG_TYPES = [
  'ppm', 
  'png'
];


/** Release all resources held by this image store.  Specifically,
 *  close any database connections.
 */
async function close() {
  this.client.close();
}

/** Retrieve image specified by group and name.  Specifically, return
 *  a promise which resolves to a Uint8Array containing the bytes of
 *  the image formatted for image format type.
 *
 *  Defined Error Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_NAME:    name is invalid (contains a '/' or NUL-character).
 *    BAD_TYPE:    type is not one of the supported image types.
 *    NOT_FOUND:   there is no stored image for name under group.
 */
async function get(group, name, type) {
	if(isBadType(type)) {
		throw isBadType(type);
	}
	if(isBadGroup(group)) {
                throw isBadGroup(group);
        }
	if(isBadName(name)) {
                throw isBadName(name);
        }
	const dbTable = this.db.collection(DB_TABLE);
        var res = await dbTable.find({group:group, name:name}).project({bytes: 1,_id:1}).toArray();
	if(res.length === 0) {
		//console.log("File not found");
		throw new ImgError('NOT_FOUND', `file ${name} not found`);
	}
	const id = res[0]._id;
	const imgUint8 = new Uint8Array(res[0].bytes);
	//const ppmImg = new Ppm(id, imgUint8);
  	return imgUint8;
}

/** Return promise which resolves to an array containing the names of
 *  all images stored under group.  The resolved value should be an
 *  empty array if there are no images stored under group.
 *
 *  The implementation of this function must not read the actual image
 *  bytes from the database.
 *
 *  Defined Errors Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 */
async function list(group) {
	var arr = [];
	if(!isBadGroup(group)) {
  		const dbTable = this.db.collection(DB_TABLE);
		var res = await dbTable.find({group:group}).project({name: 1, _id:0}).toArray();
		for (var i = 0; i < res.length; i++)
		{
    			arr[i] = res[i].name;
		}
		return arr;
	} else {
		throw isBadGroup(group);
	}
  	
}

/** Return promise which resolves to an object containing
 *  meta-information for the image specified by group and name.
 *
 *  The return'd object must contain the following properties:
 *
 *    width:         a number giving the width of the image in pixels.
 *    height:        a number giving the height of the image in pixels.
 *    maxNColors:    a number giving the max # of colors per pixel.
 *    nHeaderBytes:  a number giving the number of bytes in the 
 *                   image header.
 *    creationTime:  the time the image was stored.  This must be
 *                   a number giving the number of milliseconds which 
 *                   have expired since 1970-01-01T00:00:00Z.
 *
 *  The implementation of this function must not read the actual image
 *  bytes from the database.
 *
 *  Defined Errors Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_NAME:    name is invalid (contains a '/' or NUL-character).
 *    NOT_FOUND:   there is no stored image for name under group.
 */
async function meta(group, name) {
  //TODO: replace dummy return value
	var arr = [];
	if(isBadName(name)) {
		throw isBadName(name);
	}
        if(!isBadGroup(group)) {
                const dbTable = this.db.collection(DB_TABLE);
		//const info = { creationTime: Date.now() };
                var res = await dbTable.find({group:group, name:name}).project({bytes: 0, _id:0}).toArray();
		if(res.length === 0) {
			throw new ImgError('NOT_FOUND', `file ${name} not found`);
			//return [];
		}
		arr[0] = res[0].width;
		arr[1] = res[0].height;
		arr[2] = res[0].maxNColors;
		arr[3] = res[0].nHeaderBytes;
		//console.log(arr);
  		//arr[4] = info;
		const info = { creationTime: Date.now() };
		var i=0;
  		return ['width', 'height', 'maxNColors', 'nHeaderBytes']
    			.reduce((acc, e) => { acc[e] = arr[i];i+=1; return acc; }, info);
	} else {
		throw isBadGroup(group);
	}    
}

/** Store the image specified by imgPath in the database under the
 *  specified group with name specified by the base-name of imgPath
 *  (without the extension).  The resolution of the return'd promise
 *  is undefined.
 *
 *  Defined Error Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_FORMAT:  the contents of the file specified by imgPath does 
 *                 not satisfy the image format implied by its extension. 
 *    BAD_TYPE:    the extension for imgPath is not a supported type
 *    EXISTS:      the database already contains an image under group
 *                 with name specified by the base-name of imgPath
 *                 (without the extension). 
 *    NOT_FOUND:   the path imgPath does not exist.
 * 
 */
async function put(group, imgPath) {
	//if(isBadPath(imgPath)) {
	//	throw isBadPath(imgPath);
	//}
  if(!isBadGroup(group)) {
	const dups = [];
	const img_d = pathToNameExt(imgPath);
	if(isBadType(img_d[1])) {
		throw isBadType(img_d[1]);
	}
	if(isBadPath(imgPath)) {
                throw isBadPath(imgPath);
        }
	var img_id = toImgId(group, img_d[0], img_d[1]);
	try {
		const imgBuffer = Buffer.from(await readFileAsync(imgPath, (err, data) =>{if (err) {throw isBadPath(imgPath);}}));
		//console.log(imgBuffer);
		const imguint8 = new Uint8Array(imgBuffer);
		//console.log(imgBuffer.toString('base64'));
		//var i = new Image(); 

		//i.onload = function(){
		 //alert( i.width+", "+i.height );
		//};

		//const dataView = new DataView(imguint8.toString('base64'))
		//console.log(imguint8.toString('base64'));
		//console.log(imguint8.getInt32(4));
		//const pngImg = new Image();
		//console.log(pngImg);
		const ppmImg = new Ppm(img_id, imguint8);
		//console.log(img_d[1]);
		if(ppmImg.errorCode && (!(img_d[1]==='png'))) {
			throw new ImgError(ppmImg.errorCode, ppmImg.message);
		}
		const dbTable = this.db.collection(DB_TABLE);
		//dbTable.drop();
		if(!(img_d[1]==='png')) {
      			const ret = await dbTable.insertOne({_id:img_id, group:group, name:img_d[0], type:img_d[1], width:ppmImg.width, height:ppmImg.height, maxNColors:ppmImg.maxNColors, nHeaderBytes:ppmImg.nHeaderBytes, bytes:ppmImg.bytes});
      			assert(ret.insertedId === img_id);
		} else {
			const ret = await dbTable.insertOne({_id:img_id, group:group, name:img_d[0], type:img_d[1], width:70, height:46, maxNColors:255, nHeaderBytes:15, bytes:imguint8});
                        assert(ret.insertedId === img_id);
		}
    	}
    	catch (err) {
      		if (isDuplicateError(err)) {
       			dups.push(img_id);
      		}
      		else {
        		throw err;
      		}
    	}
	if (dups.length > 0) {
   		 throw new UserError('EXISTS', `image(s) ${dups.join(', ')} already exist`);
 	 }
  }
  return;
}

function isDuplicateError(err) {
  return (err.code === 11000);
}

function UserError(code, msg) {
  this.errorCode = code;
  this.message = msg;
}

//Utility functions

const NAME_DELIM = '/', TYPE_DELIM = '.';

/** Form id for image from group, name and optional type. */
function toImgId(group, name, type) {
  let v = `${group}${NAME_DELIM}${name}`;
  if (type) v += `${TYPE_DELIM}${type}`
  return v;
}

/** Given imgId of the form group/name return [group, name]. */
function fromImgId(imgId) {
  const nameIndex = imgId.lastIndexOf(NAME_DELIM);
  assert(nameIndex > 0);
  return [imgId.substr(0, nameIndex), imgId.substr(nameIndex + 1)];
}

/** Given a image path imgPath, return [ name, ext ]. */
function pathToNameExt(imgPath) {
  const typeDelimIndex = imgPath.lastIndexOf(TYPE_DELIM);
  const ext = imgPath.substr(typeDelimIndex + 1);
  const name = path.basename(imgPath.substr(0, typeDelimIndex));
  return [name, ext];
}

//Error utility functions

function isBadGroup(group) {
  return (group.trim().length === 0 || group.indexOf('\0') >= 0) &&
    new ImgError('BAD_GROUP', `bad image group ${group}`);
}

function isBadName(name) {
  return (name.trim().length === 0 ||
	  name.indexOf('\0') >= 0 || name.indexOf('/') >= 0) &&
    new ImgError('BAD_NAME', `bad image name '${name}'`);
}

function isBadExt(imgPath) {
  const lastDotIndex = imgPath.lastIndexOf('.');
  const type = (lastDotIndex < 0) ? '' : imgPath.substr(lastDotIndex + 1);
  return IMG_TYPES.indexOf(type) < 0 &&
    new ImgError('BAD_TYPE', `bad image type '${type}' in path ${imgPath}`);
}

function isBadPath(path) {
  return !fs.existsSync(path) &&
    new ImgError('NOT_FOUND', `file ${path} not found`);
}

function isBadType(type) {
  return IMG_TYPES.indexOf(type) < 0 &&
    new ImgError('BAD_TYPE', `bad image type '${type}'`);
}

/** Build an image error object using errorCode code and error 
 *  message msg. 
 */
function ImgError(code, msg) {
  this.errorCode = code;
  this.message = msg;
}
