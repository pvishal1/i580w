#!/usr/bin/env nodejs

'use strict';

const Ppm = require('./ppm');

/** prefix which always precedes actual message when message is hidden
 *  in an image.
 */
const STEG_MAGIC = 'stg';

/** Constructor which takes some kind of ID and a Ppm image */
function StegModule(id, ppm) {
	this.id = id;
	this.ppm = ppm;
}



/** Hide message msg using PPM image contained in this StegModule object
 *  and return an object containing the new PPM image.
 *
 *  Specifically, this function will always return an object.  If an
 *  error occurs, then the "error" property of the return'd object
 *  will be set to a suitable error message.  If everything ok, then
 *  the "ppm" property of return'd object will be set to a Ppm image
 *  ppmOut which is derived from this.ppm with msg hidden.
 *
 *  The ppmOut image will be formed from the image contained in this
 *  StegModule object and msg as follows.
 *
 *    1.  The meta-info (header, comments, resolution, color-depth)
 *        for ppmOut is set to that of the PPM image contained in this
 *        StegModule object.
 *
 *    2.  A magicMsg is formed as the concatenation of STEG_MAGIC,
 *        msg and the NUL-character '\0'.
 *
 *    3.  The bits of the character codes of magicMsg including the
 *        terminating NUL-character are unpacked (MSB-first) into the
 *        LSB of successive pixel bytes of the ppmOut image.  Note
 *        that the pixel bytes of ppmOut should be identical to those
 *        of the image in this StegModule object except that the LSB of each
 *        pixel byte will contain the bits of magicMsg.
 *
 *  The function should detect the following errors:
 *
 *    STEG_TOO_BIG:   The provided pixelBytes array is not large enough 
 *                    to allow hiding magicMsg.
 *    STEG_MSG:       The image contained in this StegModule object may already
 *                    contain a hidden message; detected by seeing
 *                    this StegModule object's underlying image pixel bytes
 *                    starting with a hidden STEG_MAGIC string.
 *
 * Each error message must start with the above IDs (STEG_TOO_BIG, etc).
 */
StegModule.prototype.hide = function(msg) {
	//TODO: hide STEG_MAGIC + msg + '\0' into a copy of this.ppm
	let ppmBytes = new Ppm(this.ppm);
	if(checkIfMagicPresent(ppmBytes)) {
		return { error: 'STEG_MSG: '+this.id+': image already contains a hidden message'};
	} else {
		var maxMsgSize = ((ppmBytes.width*ppmBytes.height*3)/8)-3;
		var ppmPixel = ppmBytes.pixelBytes;
		if(msg.length <= (maxMsgSize-1)) {
			msg = STEG_MAGIC + msg + '\0';
			for(let i=0; i<msg.length; i++) {
				let bin = msg[i].charCodeAt(0).toString(2);
				if(bin.length < 8) {
					let len = bin.length;
					for(let j=0; j<(8 - len); j++) {
						bin = '0' + bin;
					}
				}
				let k=0;
				for(let j=(i*8); j<((i*8)+8); j++) {
					if(bin[k] === '0') {
						let lsb = (parseInt(ppmPixel[j], 2) & 1);
						ppmPixel[j] = ppmPixel[j] & ~1;
					} else if(bin[k] === '1') {
						ppmPixel[j] = ppmPixel[j] | 1;
					}
					k++;
				}
			}
			return { ppm: ppmBytes };
		} else {
			return { error: 'STEG_TOO_BIG: '+this.id+': message too big to be hidden in image' };
		}
	}
}

/**
 * checks if the image already has the Message or not
 * @param ppmBytes
 * @returns
 */
function checkIfMagicPresent(ppmBytes) {
	var ppmPixel = ppmBytes.pixelBytes;
	let msg1 = '';
	let s = '';
	var j =0;
	for(let i=0; i<=(STEG_MAGIC.length * 8); i++) {
		if((j%8 === 0) && j!==0) {
			let s1='';
			s1 += String.fromCharCode(parseInt((parseInt(s, 2).toString(16)).substr(0, 2), 16));
			msg1 = msg1+s1;
			s = '';
		}
		if(((ppmPixel[i]%2) === 0)) {
			s = s+'0';
		} else {
			s = s+'1';
		}
		j++;
	}
	if(msg1 === STEG_MAGIC) {
		return true;
	}
	return false;
}

/** Return message hidden in this StegModule object.  Specifically, if
 *  an error occurs, then return an object with "error" property set
 *  to a string describing the error.  If everything is ok, then the
 *  return'd object should have a "msg" property set to the hidden
 *  message.  Note that the return'd message should not contain
 *  STEG_MAGIC or the terminating NUL '\0' character.
 *
 *  The function will detect the following errors:
 *
 *    STEG_NO_MSG:    The image contained in this Steg object does not
 *                    contain a hidden message; detected by not
 *                    seeing this Steg object's underlying image pixel
 *                    bytes starting with a hidden STEG_MAGIC
 *                    string.
 *    STEG_BAD_MSG:   A bad message was decoded (the NUL-terminator
 *                    was not found).
 *
 * Each error message must start with the above IDs (STEG_NO_MSG, etc).
 */
StegModule.prototype.unhide = function() {
	//TODO
	var msg1 = '';
	var ppmPixel = new Ppm(this.ppm).pixelBytes;
	let s = '';
	var j =0;
	var flag = false;
	for (let i of ppmPixel) {
		if((j%8 === 0) && j!==0) {
			let s1='';
			s1 += String.fromCharCode(parseInt((parseInt(s, 2).toString(16)).substr(0, 2), 16));
			if(s === '00000000') {
				flag = true;
				break;
			}
			msg1 = msg1+s1;
			s = '';
		}
		if((i%2) === 0) {
			s = s+'0';
		} else {
			s = s+'1';
		}
		j++;
	}
	if(flag) {
		if(msg1.startsWith(STEG_MAGIC)) {
			return { msg: msg1.slice(STEG_MAGIC.length) };
		} else {
			return { error: 'STEG_NO_MSG: '+this.id+': image does not have a message' };
		}
	} else {
		return { error: 'STEG_BAD_MSG: '+this.id+': bad message' };
	}
}


module.exports = StegModule;
