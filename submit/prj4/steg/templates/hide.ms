<!DOCTYPE html>
<html>
  <head>
    <title>Hide</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Hide</h1>
    <p class="error">{{errorMsg}}</p>
<form method = "POST" enctype = "multipart/form-data" action = "/stegs/hide.html"
    <table>
    {{#images}}
    <p>
	<tr>
		<td><input name="imageRadio" type="radio" id={{value}} value={{value}} {{#selected}}checked{{/selected}}></td>
                <td><img src = {{path}}/images/inputs/{{value}}.png class="container img"></td>
	</tr>
    </p>
    {{/images}}
    </table>
<textarea name="textArea" id="textArea" placeholder="Enter message to be hidden" rows="4" cols="50">{{textGiven}}</textarea><br>
<div>
    <label for="hide">Choose file to upload</label>
    <input type="file" name="textFile" accept=".txt">
</div>
<input type="submit" value="submit">
</form>
    {{>footer}}
  </body>
</html>
