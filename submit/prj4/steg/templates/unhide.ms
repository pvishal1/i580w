<!DOCTYPE html>
<html>
  <head>
    <title>Unhide</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Unhide</h1>
    <p class="error">{{errorMsg}}</p>
<form method = "POST" enctype = "multipart/formdata" action = "/stegs/unhide.html"
    {{#images}}
    <p>
	<table>
        <tr>
		<td><input name="imageRadio" type="radio" id={{.}} value={{.}} {{#selected}}selected{{/selected}}></td>
                <td><img src = {{path}}/images/steg/{{.}}.png class="container img"></td>
        </tr>
	</table>
    </p>
    {{/images}}
<input type="submit" value="submit">
</form>
    {{>footer}}
  </body>
</html>
