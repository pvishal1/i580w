<!DOCTYPE html>
<html>
  <head>
    <title>Hide</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>{{success}}</h1>
    <p class="error">{{errorMsg}}</p>
    {{>footer}}
  </body>
</html>
