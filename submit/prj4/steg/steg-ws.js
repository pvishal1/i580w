'use strict';

const axios = require('axios');


function StegWs(baseUrl) {
  this.stegsUrl = `${baseUrl}`;
}

module.exports = StegWs;

StegWs.prototype.list = async function() {
  try {
    const url = this.stegsUrl + '/images/inputs';
    const response = await axios.get(url);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.putMsg = async function(name, msg) {
  try {
    const url = this.stegsUrl + '/steg/inputs/'+name;
    const response = await axios.post(url, {outGroup:"steg", msg:msg});
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.listHiddenImg = async function() {
  try {
    const url = this.stegsUrl + '/images/steg';
	//console.log(url);
    const response = await axios.get(url);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.getMsg = async function(name) {
  try {
    const url = this.stegsUrl + '/steg/steg/'+name;
    //console.log(url);
    const response = await axios.get(url,"steg", name);
    //console.log(response.data);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};
