'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const mustache = require('mustache');
const querystring = require('querystring');

const multer = require('multer');
const upload = multer();


const STATIC_DIR = 'statics';
const TEMPLATES_DIR = 'templates';

function serve(port, base, model) {
  const app = express();
  app.locals.port = port;
  app.locals.base = base;
  app.locals.model = model;
  process.chdir(__dirname);
  app.use(base, express.static(STATIC_DIR));
  setupTemplates(app);
  setupRoutes(app);
  app.listen(port, function() {
    console.log(`listening on port ${port}`);
  });
}


module.exports = serve;

/******************************** Routes *******************************/

function setupRoutes(app) {
  const base = app.locals.base;
  app.get(`${base}/hide.html`, doHide(app));
  //app.use(bodyParser.urlencoded({ extended: false }));
  app.post(`${base}/hide.html`, bodyParser.urlencoded({extended: false}),
           upload.single('textFile'),actualHide(app));
  app.get(`${base}/unhide.html`, doUnhide(app));
  app.post(`${base}/unhide.html`, bodyParser.urlencoded({extended: false}), actualUnhide(app));
}

/*************************** Action Routines ***************************/
function doHide(app) {
  return async function(req, res) {
      let steg;
      let model;
      try {
        steg = await app.locals.model.list();
	for(let i=0;i< steg.length;i++){
		steg[i] = {value: steg[i]};
	}
      }
      catch (err) {
        model = {errorMessage:"Image found Error", app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base};
      }
      model = {images: steg, app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base};
      const html = doMustache(app, 'hide', model);
      res.send(html);
  };
};

function actualHide(app) {
  return async function(req, res) {
      let steg;
      let model;
      let html, imgSel, textGiven='';
      let flag = true; 
      try {
	if(typeof req.body.imageRadio === 'undefined') {
		if(req.body.textArea !== '') {
			textGiven = req.body.textArea;
		}
		flag = false;
		throw "Select the Image";
	}
	else if(req.body.textArea === '') {
		if(req.file) {
			flag=true;
		} else { 
			imgSel = req.body.imageRadio;
			flag = false;
			throw "Add message to be hidden";
		}
	}
	if(flag) {
		if(req.body.textArea === '') {
			let val = req.file.buffer.toString('ascii', 0, req.file.buffer.length);
			await app.locals.model.putMsg(req.body.imageRadio, val);
		} else {
			await app.locals.model.putMsg(req.body.imageRadio, req.body.textArea);
		}
		model = {images: steg, app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base, success:"Message hide successful" };
		html = doMustache(app, 'success', model);
	}
      }
      catch (err) {
	steg = await app.locals.model.list();
	for(let i=0;i< steg.length;i++) {
		if(i==imgSel) { 
			steg[i] = {value: steg[i], selected: true};
		}
		else {
			steg[i] = {value: steg[i]};
		}
	}
        model = {images:steg, errorMsg:err, app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base, errors:err, textGiven:textGiven};
	html = doMustache(app, 'hide', model);
      }
      res.send(html);
  };
};

function doUnhide(app) {
  return async function(req, res) {
      let steg;
      let model;
      try {
        steg = await app.locals.model.listHiddenImg();
      }
      catch (err) {
        model = {errorMessage:"Image found Error", app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base};
      }
      model = {images: steg, app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base};
      const html = doMustache(app, 'unhide', model);
      res.send(html);
  };
};

function actualUnhide(app) {
  return async function(req, res) {
      let msg;
      let model;
      let html;
      try {
        if(typeof req.body.imageRadio === 'undefined') {
                throw "Select the Image"; 
        }  

        msg = await app.locals.model.getMsg(req.body.imageRadio);
	model = {app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base, success:"Hidden Message: "+msg.msg };
	html = doMustache(app, 'success', model);
      }
      catch (err) {
	let steg = await app.locals.model.listHiddenImg();
        model = {images: steg, app: `${app}`, path: app.locals.model.stegsUrl, base: app.locals.base, errorMsg:err};
	html = doMustache(app, 'unhide', model);
      }
      res.send(html);
  };
};

/** Return a model suitable for mixing into a template */
function errorModel(app, values={}, errors={}) {
  return {
    base: app.locals.base,
    errors: errors._,
  };
}

/************************ General Utilities ****************************/

/** Decode an error thrown by web services into an errors hash
 *  with a _ key.
 */
function wsErrors(err) {
  const msg = (err.message) ? err.message : 'web service error';
  console.error(msg);
  return { _: [ msg ] };
}

function doMustache(app, templateId, view) {
  const templates = { footer: app.templates.footer };
  return mustache.render(app.templates[templateId], view, templates);
}

function errorPage(app, errors, res) {
  if (!Array.isArray(errors)) errors = [ errors ];
  const html = doMustache(app, 'errors', { errors: errors });
  res.send(html);
}

function isNonEmpty(v) {
  return (typeof v !== 'undefined') && v.trim().length > 0;
}

function setupTemplates(app) {
  app.templates = {};
  for (let fname of fs.readdirSync(TEMPLATES_DIR)) {
    const m = fname.match(/^([\w\-]+)\.ms$/);
    if (!m) continue;
    try {
      app.templates[m[1]] =
	String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
    }
    catch (e) {
      console.error(`cannot read ${fname}: ${e}`);
      process.exit(1);
    }
  }
}

